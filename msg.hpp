#include "user.hpp"
#include <cstring>
#include <csignal>
#include <sys/types.h>
#ifndef MSG_HPP
#define MSG_HPP
void broadcast(user* self,const char* msg,pid_t serverpid);
void unicast(user* self,const char* msg,int toid,pid_t serverpid);
#endif

