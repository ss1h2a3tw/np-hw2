CXX := g++

CXX_FLAGS += -O2 -Wall -Wextra -std=c++14

SRC_FILES := builtin.cpp exec.cpp shell.cpp numberpipe.cpp parser.cpp tmctl.cpp chldctl.cpp server.cpp msg.cpp

HEADER_FILES := builtin.hpp cmd.hpp exec.hpp numberpipe.hpp parser.hpp tmctl.hpp chldctl.hpp shell.hpp msg.hpp

TARGET := server

.phony: all

all: $(TARGET)

$(TARGET): $(SRC_FILES) $(HEADER_FILES)
	$(CXX) $(CXX_FLAGS) $(SRC_FILES) -o $(TARGET)
