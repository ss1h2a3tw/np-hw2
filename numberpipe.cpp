#include <string>
#include <list>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
using namespace std;
const string mktemp_template = "/tmp/myshell.XXXXXX";
using np_type=std::list<std::pair<size_t,std::string>>;
int chkinnp(const np_type& np){
    if(np.size()==0)return STDIN_FILENO;
    if(np.begin()->first!=0)return STDIN_FILENO;
    int fdin=open(np.begin()->second.c_str(),O_RDONLY);
    if(fdin==-1)throw("Failed openning tempfile");
    return fdin;
}
void decnp(np_type& np){
    if(np.size()==0)return;
    if(np.begin()->first==0){
        if(remove(np.begin()->second.c_str())==-1)throw("Error in remove numberpipe tmpfile");
        np.pop_front();
    }
    for(auto &x:np)x.first--;
}
int outnp(np_type& np,size_t cnt){
    auto it = np.begin();
    for(;it!=np.end()&&it->first<cnt;it++);
    if(it!=np.end()&&it->first==cnt){
        int fd=open(it->second.c_str(),O_WRONLY|O_CREAT|O_APPEND,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
        if(fd==-1)throw("Failed to open tmpfile to write");
        return fd;
    }
    char tmp[mktemp_template.size()+1];
    strcpy(tmp,mktemp_template.c_str());
    int fdout = mkstemp(tmp);
    if(fdout==-1)throw("Failed to create tmpfile");
    np.insert(it,{cnt,string(tmp)});
    return fdout;
}
void delnp(np_type& np){
    for(auto x = np.begin();x!=np.end();){
        if(remove(x->second.c_str())==-1)throw("Error in remove numberpipe tmpfile");
        x++;
        np.pop_front();
    }
}
