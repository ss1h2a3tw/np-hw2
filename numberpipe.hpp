#include <string>
#include <list>
#include <utility>
#ifndef NUMBERPIPE_HPP
#define NUMBERPIPE_HPP
using np_type=std::list<std::pair<size_t,std::string>>;
int chkinnp(const np_type& np);
void decnp(np_type& np);
int outnp(np_type& np,size_t cnt);
void delnp(np_type& np);
#endif
