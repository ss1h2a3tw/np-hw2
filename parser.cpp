#include "cmd.hpp"
#include <vector>
#include <algorithm>
using namespace std;
static void checkslash(const command& now){
    for(auto& x:now.args){
        if(any_of(x.begin(),x.end(),[](char x){return x=='/';}))throw("For Security Reasons no / in arguments permitted");
    }
}
static void checkredirect(const command& now){
    for(auto& x:now.redirects){
        for(auto& y:SEPR){
            if(x.path==y)throw("Expect from/to");
            //TODO single quote
        }
    }
}
static size_t expand(string& in,size_t now , size_t pred){
    string name = in.substr(pred+1,now-pred-1);
    in.erase(pred,now-pred);
    auto env = getenv(name.c_str());
    if(env!=NULL)in.insert(pred,getenv(name.c_str()));
    return pred;
}
static vector<string> gettokens(string& in ){
    vector<string> tokens;
    size_t pre = 0;
    size_t pred = 0;
    bool needexpand=false;
    for(size_t i = 0 ; i < in.size() ; i ++){
        if(in[i]=='$'){
            if(needexpand){
                i=expand(in,i,pred);
                needexpand=false;
                continue;
            }
            pred=i;
            needexpand=true;
        }
        if(isspace(in[i])){
            if(needexpand){
                i=expand(in,i,pred);
                needexpand=false;
                continue;
            }
            if(i-pre>0)tokens.push_back(in.substr(pre,i-pre));
            pre=i+1;
            continue;
        }
        for(auto& x:SEPR){
            if(in.compare(i,x.size(),x)==0){
                if(needexpand){
                    i=expand(in,i,pred);
                    needexpand=false;
                    break;
                }
                if(i-pre>0){
                    tokens.push_back(in.substr(pre,i-pre));
                }
                tokens.push_back(in.substr(i,x.size()));
                i+=x.size()-1;
                pre=i+1;
                break;
            }
        }

    }
    return tokens;
}

static command formcommand(const vector<string>::const_iterator begin , const vector<string>::const_iterator end){
    command now;
    now.clear();
    for(auto it = begin ; it != end ; it++){
        bool getredir=false;
        for(size_t i = 0 ; i < RDIR.size() ; i ++){
            if(*it==RDIR[i]){
                if(it==begin)throw("Expect command but got redirect");
                if(it+1==end)throw("No redirect from/to");
                redirect tmp;
                tmp.type=(rdir_type)i;
                tmp.path = *(it+1);
                if(all_of(tmp.path.begin(),tmp.path.end(),[](char x){return x>='0'&&x<='9';})&&tmp.type==OUT)
                    tmp.type=REMOTE_OUT;
                if(all_of(tmp.path.begin(),tmp.path.end(),[](char x){return x>='0'&&x<='9';})&&tmp.type==IN)
                    tmp.type=REMOTE_IN;
                now.redirects.push_back(move(tmp));
                it++;
                getredir=true;
                break;
            }
        }
        if(getredir)continue;
        if(it==begin){
            now.exe = *it;
            for(size_t i = 0 ; i < BLTIN.size() ; i ++){
                if(now.exe==BLTIN[i]){
                    now.builtin=true;
                    now.builtin_type=(bltn_type)i;
                }
            }
        }
        else{
            now.args.push_back(*it);
        }
    }
    checkredirect(now);
    checkslash(now);
    return now;
}

static vector<command> getcommands(const vector<string>& tokens){
    auto pre = tokens.begin();
    vector<command> now;
    for(auto it = tokens.begin() ; it != tokens.end() ; it++){
        for(size_t i = 0 ; i < PIPE.size() ; i ++){
            if(PIPE[i]==*it){
                if(it==pre)throw("Expect command but got pipe");
                command tmp=formcommand(pre,it);
                tmp.piped=true;
                tmp.piped_type=(pipe_type)i;
                now.push_back(move(tmp));
                pre=it+1;
            }
            if(all_of(pre->cbegin(),pre->cend(),[](char c){return c>='0'&&c<='9';})){
                int tmp=stoi(*pre);
                if(tmp<=0)throw("Invalid number of numberpipe");
                (now.end()-1)->pipecnt=(size_t)tmp;
                pre++;
                it++;
            }
            continue;
        }
        if(*it=="&"){
            if(pre==it)throw("Expect command but found &");
            command tmp=formcommand(pre,it);
            tmp.piped=false;
            tmp.background=true;
            now.push_back(move(tmp));
            pre=it+1;
            continue;
        }
        if(*it==";"){
            if(pre==it){
                pre=it+1;
                continue;
            }
            command tmp=formcommand(pre,it);
            tmp.piped=false;
            now.push_back(move(tmp));
            pre=it+1;
            continue;
        }
    }
    return now;
}
static vector<vector<command>> pseperate(const vector<command>& commands){
    vector<vector<command>> rtn;
    vector<command> tmp;
    for(auto& now :commands){
        if(tmp.size()!=0&&now.builtin==true)throw("Find buildin commands in pipe");
        if(now.piped&&now.builtin==true)throw("Find builtin commands in pipe");
        tmp.push_back(now);
        if(!now.piped||(now.piped&&now.pipecnt>0)){
            rtn.push_back(tmp);
            tmp.clear();
        }
    }
    if(tmp.size()!=0){
        throw("Expect commands after non number pipes");
    }
    return rtn;
}
vector<vector<command>> getpipesepcmd(string &in){
    in = in + ' ';
    vector<string> tokens=gettokens(in);
    tokens.push_back(";");
    vector<command> commands=getcommands(tokens);
    vector<vector<command>> pscommands=pseperate(commands);
    return pscommands;
}
