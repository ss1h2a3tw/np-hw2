#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <cctype>
#include <cstdio>
#include "cmd.hpp"
#include "builtin.hpp"
#include "exec.hpp"
#include "numberpipe.hpp"
#include "parser.hpp"
#include "tmctl.hpp"
#include "chldctl.hpp"
//Using C++14

using namespace std;

//const string prompt=">";
const string prompt="% "; //NP
const string FIFOPREFIX = "/tmp/chenshh-";

void deletefifos(const vector<command> &cmds,int id){
    for(auto cmd:cmds){
        for(auto re:cmd.redirects){
            if(re.type==REMOTE_IN){
                int fromid=stoi(re.path)-1;
                string fifopath=FIFOPREFIX+to_string(fromid)+"-"+to_string(id);
                remove(fifopath.c_str());
            }
        }
    }
}

int shell (user* users,int id,pid_t serverpid){
    signal(SIGINT,[](int){
        cout<<"^C"<<endl;
    });
    signal(SIGCHLD,sigchldhandler);
    signal(SIGTTOU,SIG_IGN);
    if(setpgid(0,getpid())==-1){
        cerr << "Failed to set pgid" << endl;
        exit(EXIT_FAILURE);
    }
    chkterm();
    np_type np;
    while(1){
        try{
            cout.flush();
            cout << prompt;
            string in;
            getline(cin,in);
            string fullline=in;
            while(isspace(*(fullline.end()-1))){
                fullline.erase(fullline.end()-1);
            }
            vector<vector<command>> pscommands=getpipesepcmd(in);
            for(auto& cmds:pscommands){
                bool background=cmds[cmds.size()-1].background;
                decnp(np);
                int fdin=chkinnp(np);
                int fdout=STDOUT_FILENO;
                if((cmds.end()-1)->pipecnt>0){
                    fdout=outnp(np,(cmds.end()-1)->pipecnt);
                }
                if(ckrunbuiltin(cmds[0],np,users,id,serverpid))continue;
                pid_t pgid;
                if(cmds.size()==1)pgid=execsingle(cmds,fdin,fdout,users,id,serverpid,fullline);
                else pgid=execpipe(cmds,fdin,fdout,users,id,serverpid,fullline);
                if(fdin!=STDIN_FILENO)close(fdin);
                if(fdout!=STDOUT_FILENO)close(fdout);
                if(!background){
                    settermpgid(pgid);
                    while(1){
                        if(waitpid(pgid*-1,NULL,0)==-1&&errno==ECHILD)break;
                    }
                    deletefifos(cmds,id);
                    settermpgid(getpid());
                }
                else insertchld(pgid,cmds[0].exe);
            }
        }
        catch (const char *err){
            cerr << err << endl;
        }
        catch (const string& err){
            cerr << err << endl;
        }
    }
}
