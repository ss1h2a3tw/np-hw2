#include <cstdint>
#include <sys/types.h>
#ifndef USER_HPP
#define USER_HPP
const int NAME_LENGTH_MAX=29;
const int ADDR_LENGTH_MAX=19;
const int MSG_LENGTH_MAX=2047;
struct user{
    int sockfd;
    char name[30];
    char addr[20];
    uint16_t port;
    pid_t pid;
    char msg_ct[10][2048];
    int msg_to[10];

};
#endif
