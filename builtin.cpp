#include "cmd.hpp"
#include "numberpipe.hpp"
#include "chldctl.hpp"
#include "tmctl.hpp"
#include "user.hpp"
#include "msg.hpp"
#include <cstdlib>
#include <vector>
#include <unistd.h>
#include <csignal>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sys/wait.h>
#include <cstring>

using namespace std;
static void killchild(){
    signal(SIGTERM,SIG_IGN);
    kill(getpid(),SIGTERM);
    exit(EXIT_SUCCESS);
}

static void builtin_cd(const command& cmd){
    if(cmd.args.size()==0){
        const char* homepath = getenv("HOME");
        if(homepath==NULL)throw("Failed to get home path");
        if(chdir(getenv("HOME"))==-1)throw("Error in chdir()");
        return;
    }
    if(cmd.args.size()>1)throw("cd:Too many arguments");
    if(chdir(cmd.args[0].c_str())==-1)throw("Error in chdir()");
}

static void builtin_setenv(const command& cmd){
    if(cmd.args.size()!=2)throw("Usage: setenv name value");
    if(setenv(cmd.args[0].c_str(),cmd.args[1].c_str(),1)==-1)throw("Error in setenv()");
}

static void builtin_unset(const command& cmd){
    if(cmd.args.size()!=1)throw("Usage: unset name");
    if(unsetenv(cmd.args[0].c_str())==-1)throw("Error in unsetenv()");
}

static void builtin_printenv(const command& cmd){
    if(cmd.args.size()!=1)throw("Usage: printenv name");
    char* tmp=getenv(cmd.args[0].c_str());
    if(tmp==NULL)throw("No such Variable");
    cout << cmd.args[0] << "=" << tmp << endl;
}

static void builtin_fg(const command& cmd){
    auto& args = cmd.args;
    if(!isinteractive())throw("fg is not available in non interactive mode");
    if(cmd.args.size()!=1)throw("Usage: fg Job num");
    if(!all_of(args[0].begin(),args[0].end(),[](char c){return c>='0'&&c<='9';}))
        throw("Usage: fg Job num");
    int i = stoi(args[0]);
    pair<pid_t,string> chld=getchld(i-1);
    cout << "Sent Job num " <<i<<" "<<chld.second << " to foreground" << endl;
    settermpgid(chld.first);
    kill(-chld.first,SIGCONT);
    while(1){
        waitpid(-chld.first,NULL,0);
        if(errno==ECHILD)break;
    }
    settermpgid(getpid());
}

static void builtin_exit(np_type& np){
    delnp(np);
    killchild();
    exit(EXIT_SUCCESS);
}

static void builtin_who(const user* users,int id){
    for(int i = 0 ; i < 40 ; i ++){
        if(users[i].sockfd){
            cout << setw(7) << left << i+1 << " "
                 << setw(20) << left << users[i].name << " "
                 << setw(28) << left << string(users[i].addr)+"/"+to_string(users[i].port) << " "
                 << setw(10) << left << users[i].pid << " ";
            if(id==i)cout << "<- me";
            cout << endl;
        }
    }
}

static void builtin_name(const command& cmd,user* users,int id){
    if(cmd.args.size()!=1)throw("Usage name username");
    for(int i = 0 ; i < 40 ; i ++){
        if(users[i].sockfd&&i!=id){
            if(strncmp(cmd.args[0].c_str(),users[i].name,NAME_LENGTH_MAX)==0){
                string err=string("*** User ")+cmd.args[0]+" already exists. ***";
                throw(err);
            }
        }
    }
    strncpy(users[id].name,cmd.args[0].c_str(),NAME_LENGTH_MAX);
}

static void builtin_tell(const command& cmd,user* users,int id,pid_t serverpid){
    if(cmd.args.size()<2)throw("Usage: tell destid msg1 [msg2...]");
    int toid;
    try{toid=stoi(cmd.args[0])-1;}
    catch (const invalid_argument& ia){
        throw(ia.what());
    }
    catch (const out_of_range& oor){
        throw(oor.what());
    }
    if(id<0||id>=40||!users[toid].sockfd)throw("Not a valid id");
    string tmp;
    for(size_t i = 1 ; i < cmd.args.size() ; i ++){
        tmp+=cmd.args[i];
        tmp+=" ";
    }
    tmp = string("*** ")+users[id].name+" told you ***: "+tmp+"\n";
    unicast(users+id,tmp.c_str(),toid,serverpid);
}

static void builtin_yell(const command& cmd,user* users,int id,pid_t serverpid){
    if(cmd.args.size()==0)throw("Usage: yell msg1 [msg2...]");
    string tmp;
    for(size_t i = 0 ; i < cmd.args.size() ; i ++){
        tmp+=cmd.args[i];
        tmp+=" ";
    }
    tmp = string("*** ")+users[id].name+" yelled ***: "+tmp+"\n";
    broadcast(users+id,tmp.c_str(),serverpid);
}

int ckrunbuiltin(const command& cmd,np_type& np,user* users,int id,pid_t serverpid){
    if(cmd.builtin){
        switch(cmd.builtin_type){
            case CD:
                builtin_cd(cmd);
                break;
            case SETENV:
                builtin_setenv(cmd);
                break;
            case UNSET:
                builtin_unset(cmd);
                break;
            case PRINTENV:
                builtin_printenv(cmd);
                break;
            case EXIT:
                builtin_exit(np);
                break;
            case FG:
                builtin_fg(cmd);
                break;
            case WHO:
                builtin_who(users,id);
                break;
            case NAME:
                builtin_name(cmd,users,id);
                break;
            case TELL:
                builtin_tell(cmd,users,id,serverpid);
                break;
            case YELL:
                builtin_yell(cmd,users,id,serverpid);
                break;
        }
        return 1;
    }
    return 0;
}
