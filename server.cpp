#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <typeinfo>
#include <cstring>
#include <cstdint>
#include <csignal>
#include "user.hpp"
#include "shell.hpp"

using namespace std;

static user *users;
static pid_t ownpid;
static int listenfd;

const string defaultname="(no name)";
const string FIFOPREFIX = "/tmp/chenshh-";

void execshell(int fd,int id){
    string home=getenv("HOME");
    //if(setenv("PATH","bin:.",1)==-1)throw("Error in setenv()");
    if(dup2(fd,STDIN_FILENO)==-1)throw("Error in dup2()");
    if(dup2(fd,STDOUT_FILENO)==-1)throw("Error in dup2()");
    if(dup2(fd,STDERR_FILENO)==-1)throw("Error in dup2()");
    cout << "****************************************" << endl;
    cout << "** Welcome to the information server. **" << endl;
    cout << "****************************************" << endl;
    //if(chdir((home+"/ras").c_str())==-1)throw("Error in chdir()");
    close(fd);
    shell(users,id,ownpid);
    //Prevent it doesnt exit
    exit(EXIT_FAILURE);
}

void resetusers(){
    for(int i = 0 ; i < 40 ; i ++){
        users[i].sockfd=0;
        for(int j = 0 ; j < 10 ; j ++){
            users[i].msg_to[j]=-2;
        }
    }
}

int insertuser(int sockfd, const char* addr, uint16_t port){
    for(int i = 0 ; i < 40 ; i ++){
        if(!users[i].sockfd){
            users[i].sockfd=sockfd;
            strncpy(users[i].name,defaultname.c_str(),defaultname.size());
            strncpy(users[i].addr,addr,ADDR_LENGTH_MAX);
            users[i].port=port;
            return i;
        }
    }
    return -1;
}

void deletetouser(int id){
    for(int i = 0 ; i < 40 ; i ++){
        string path = FIFOPREFIX+to_string(i)+"-"+to_string(id);
        remove(path.c_str());
    }
}

void onexit(int){
    pid_t exitpid=waitpid(-1,NULL,0);
    printf("Get exit of pid %d\n",exitpid);
    for(int i = 0 ; i < 40 ; i ++){
        if(users[i].sockfd&&users[i].pid==exitpid){
            shutdown(users[i].sockfd,SHUT_RDWR);
            close(users[i].sockfd);
            users[i].sockfd=0;
            deletetouser(i);
            for(int j = 0 ; j < 10 ; j ++){
                users[i].msg_to[j]=-2;
            }
            return;
        }
    }
}

void msg_brd(const char* msg){
    cout << "Broadcasting" << msg ;
    for(int i = 0 ; i < 40 ; i ++){
        if(users[i].sockfd){
            write(users[i].sockfd,msg,strlen(msg));
        }
    }
}

void msg_uni(int id,const char* msg){
    if(id<0||id>=40||users[id].sockfd==0)return;
    write(users[id].sockfd,msg,strlen(msg));
}

void onsig1(int){
    for(int i = 0 ; i < 40 ; i ++){
        if(users[i].sockfd){
            for(int j = 0 ; j < 10 ; j ++){
                if(users[i].msg_to[j]!=-2){
                    //Prevent no null terminator
                    users[i].msg_ct[j][MSG_LENGTH_MAX]=0;
                    if(users[i].msg_to[j]==-1){
                        msg_brd(users[i].msg_ct[j]);
                    }
                    else{
                        msg_uni(users[i].msg_to[j],users[i].msg_ct[j]);
                    }
                    users[i].msg_to[j]=-2;
                }
            }
        }
    }
}

void msg_newuser(int id){
    for(int i = 0 ; i < 40 ; i ++){
        if(users[i].sockfd&&i!=id){
            string msg = string("*** User '")+users[id].name+"' entered from "+users[id].addr+"/"+to_string(users[id].port)+". ***\n";
            write(users[i].sockfd,msg.c_str(),msg.size());
        }
    }
}

int main (){
    try{
        ownpid=getpid();
        signal(SIGCHLD,onexit);
        signal(SIGUSR1,onsig1);
        signal(SIGINT,[](int){
                shutdown(listenfd,SHUT_RDWR);
        });
        int fd=socket(AF_INET,SOCK_STREAM,0);
        listenfd=fd;
        sockaddr_in addr;
        addr.sin_family=AF_INET;
        addr.sin_port=htons(8787);
        addr.sin_addr.s_addr=INADDR_ANY;
        if(fd==-1)throw("Error opening socket");
        if(bind(fd,(sockaddr*)&addr,sizeof(sockaddr_in))==-1)throw("Failed to bind socket");
        int shmkey=shmget(IPC_PRIVATE,sizeof(user)*40,IPC_CREAT|IPC_EXCL|0600);
        if(shmkey==-1)throw("Failed to get shared memory");
        users=(user*)shmat(shmkey,NULL,0);
        if(users==(void*)-1)throw("Failed to attach shared memory");
        //Mark it as removed first
        if(shmctl(shmkey,IPC_RMID,NULL)==-1)throw("Failed to mark shm as removed");
        resetusers();
        while(1){
            listen(fd,10);
            memset(&addr,0,sizeof(sockaddr_in));
            socklen_t clilen=sizeof(sockaddr_in);
            int newfd=accept(fd,(sockaddr*)&addr,&clilen);
            if(newfd==-1)throw("Failed to accept new client");
            int newid=insertuser(newfd,inet_ntoa(addr.sin_addr),ntohs(addr.sin_port));
            if(newid==-1){
                cout << "User full, closing new connection" << endl;
                close(newfd);
                continue;
            }
            cout << "New user with fd: " << newfd << " id: " << newid+1 << " name: " << users[newid].name
                 << " addr: " << users[newid].addr << " port: " << users[newid].port << endl;
            pid_t pid=fork();
            if(pid==-1)throw("Failed to fork");
            if(pid==0){
                close(fd);
                execshell(newfd,newid);
            }
            else {
                users[newid].pid=pid;
                msg_newuser(newid);
            }
        }
    }
    catch(const char* err){
        cerr << err << endl;
    }
}
