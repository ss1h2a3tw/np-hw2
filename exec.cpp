#include <iostream>
#include <vector>
#include <csignal>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include "cmd.hpp"
#include "user.hpp"
#include "msg.hpp"
using namespace std;

const string FIFOPREFIX = "/tmp/chenshh-";
static void simpleexec(const command& cmd,user* users,int id,pid_t serverpid,const string& fullline){
    try{
        signal(SIGCHLD,SIG_DFL);
        char* argv[cmd.args.size()+2];
        argv[0] = new char[cmd.exe.size()];
        argv[cmd.args.size()+1]=NULL;
        strncpy(argv[0],cmd.exe.c_str(),cmd.exe.size());
        argv[0][cmd.exe.size()]=0;
        for(size_t i = 0 ; i < cmd.args.size() ; i ++){
            argv[i+1] = new char[cmd.args[i].size()+1];
            strncpy(argv[i+1],cmd.args[i].c_str(),cmd.args[i].size());
            argv[i+1][cmd.args[i].size()]=0;
        }
        for(size_t i = 0 ; i < cmd.redirects.size() ; i ++){
            int newfd=0;
            string fifopath;
            switch(cmd.redirects[i].type){
                case APPEND_OUT:
                    newfd=open(cmd.redirects[i].path.c_str(),O_WRONLY|O_CREAT|O_APPEND,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                    if(newfd==-1)throw("Failed to open file");
                    dup2(newfd,STDOUT_FILENO);
                    close(newfd);
                    break;
                case DUP_OUT:
                    break;
                case DUP_IN:
                    break;
                case OUT:
                    newfd=open(cmd.redirects[i].path.c_str(),O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                    if(newfd==-1)throw("Failed to open file");
                    dup2(newfd,STDOUT_FILENO);
                    close(newfd);
                    break;
                case IN:
                    newfd=open(cmd.redirects[i].path.c_str(),O_RDONLY);
                    if(newfd==-1)throw("Failed to open file");
                    dup2(newfd,STDIN_FILENO);
                    close(newfd);
                    break;
                case REMOTE_OUT:
                    int toid;
                    try{toid=stoi(cmd.redirects[i].path)-1;}
                    catch (const invalid_argument& ia){
                        throw(ia.what());
                    }
                    catch (const out_of_range& oor){
                        throw(oor.what());
                    }
                    if(toid<0||toid>=40||!users[toid].sockfd){
                        string tmp="*** Error: user #"+to_string(toid+1)+" does not exist yet. ***";
                        throw(tmp);
                    }
                    fifopath = FIFOPREFIX + to_string(id) + "-" + to_string(toid);
                    newfd=open(fifopath.c_str(),O_WRONLY|O_CREAT|O_EXCL,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                    if(newfd==-1){
                        string tmp="*** Error: the pipe #"+to_string(id+1)+"->#"+to_string(toid+1)+" already exists. ***";
                        throw(tmp);
                    }
                    dup2(newfd,STDOUT_FILENO);
                    close(newfd);
                    {
                        string tmp=string("*** ")+users[id].name+" (#"+to_string(id+1)+") just piped '"+fullline+
                                   "' to "+users[toid].name+" (#"+to_string(toid+1)+") ***\n";
                        broadcast(users+id,tmp.c_str(),serverpid);
                    }
                    break;
                case REMOTE_IN:
                    int fromid;
                    try{fromid=stoi(cmd.redirects[i].path)-1;}
                    catch (const invalid_argument& ia){
                        throw(ia.what());
                    }
                    catch (const out_of_range& oor){
                        throw(oor.what());
                    }
                    if(fromid<0||fromid>=40||!users[fromid].sockfd){
                        string tmp="*** Error: user #"+to_string(fromid)+" does not exist yet. ***";
                        throw(tmp);
                    }
                    fifopath = FIFOPREFIX + to_string(fromid) + "-" + to_string(id);
                    newfd=open(fifopath.c_str(),O_RDONLY);
                    if(newfd==-1){
                        string tmp="*** Error: the pipe #"+to_string(fromid)+"->#"+to_string(id)+" does not exist yet. ***";
                        throw(tmp);
                    }
                    dup2(newfd,STDIN_FILENO);
                    close(newfd);
                    {
                        string tmp=string("*** ")+users[id].name+" (#"+to_string(id+1)+") just received the pipe from "+
                                   +users[fromid].name+" (#"+to_string(fromid+1)+") by '"+fullline+"' ***\n";
                        broadcast(users+id,tmp.c_str(),serverpid);
                    }
                    break;
            }
        }
        int rtn=execvp(cmd.exe.c_str(),argv);
        for(size_t i = 0 ; i <= cmd.args.size() ; i ++){
            delete argv[i];
        }
        if(rtn==-1){
            if(errno==ENOENT){
                throw("Unknown command: ["+cmd.exe+"].");
            }
            throw("Error while executing commands");
        }
    }
    catch (const char* err){
        cerr << err << endl;
    }
    catch (const string& err){
        cerr << err << endl;
    }
    // If we get here , it means we didn't exec normally
    exit(EXIT_FAILURE);
}

static pid_t modfdfork(const command& cmd,int pgid,int fdin,int fdout,int fderr ,user* users,int id,pid_t serverpid,const string& fullline){
    pid_t pid;
    while((pid=fork())==-1){
        wait(NULL);
    }
    if(pid==0){
        setpgid(0,pgid);
        dup2(fdin,STDIN_FILENO);
        dup2(fdout,STDOUT_FILENO);
        dup2(fderr,STDERR_FILENO);
        auto chk=[](int fd){
            if(fd!=STDIN_FILENO&&fd!=STDOUT_FILENO&&fd!=STDERR_FILENO)
                return true;
            return false;
        };
        if(chk(fdin))close(fdin);
        if(chk(fdout))close(fdout);
        if(chk(fderr))close(fderr);
        simpleexec(cmd,users,id,serverpid,fullline);
    }
    if(pgid==0)
        setpgid(pid,pid);
    else setpgid(pid,pgid);

    return pid;
}

static pid_t moderrfork(const command& cmd,int pgid, int _fdin, int _fdout,user* users,int id,pid_t serverpid,const string& fullline){
    if(cmd.piped&&cmd.piped_type==PIPE_OUTERR){
        return modfdfork(cmd,pgid,_fdin,_fdout,_fdout,users,id,serverpid,fullline);
    }
    else return modfdfork(cmd,pgid,_fdin,_fdout,STDERR_FILENO,users,id,serverpid,fullline);
};
pid_t execsingle(const vector<command>& cmds,int fdin,int fdout,user* users,int id,pid_t serverpid,const string& fullline){
    pid_t pgid=moderrfork(cmds[0],0,fdin,fdout,users,id,serverpid,fullline);
    return pgid;
}
pid_t execpipe(const vector<command>& cmds,int fdin,int fdout,user* users,int id,pid_t serverpid,const string& fullline){
    int pfd[2];
    if(pipe(pfd)==-1)throw("Failed to pipe");
    int pre=pfd[0];
    pid_t pgid=moderrfork(cmds[0],0,fdin,pfd[1],users,id,serverpid,fullline);
    close(pfd[1]);
    for(size_t i = 1 ; i < cmds.size()-1 ; i ++){
        if(pipe(pfd)==-1)throw("Failed to pipe");
        moderrfork(cmds[i],pgid,pre,pfd[1],users,id,serverpid,fullline);
        close(pre);
        close(pfd[1]);
        pre=pfd[0];
    }
    moderrfork(cmds[cmds.size()-1],pgid,pre,fdout,users,id,serverpid,fullline);
    close(pre);
    return pgid;
}
