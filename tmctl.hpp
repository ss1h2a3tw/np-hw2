#include <unistd.h>
#ifndef TMCTL_HPP
#define TMCTL_HPP
bool isinteractive();
void chkterm();
void settermpgid(pid_t pid);
#endif
