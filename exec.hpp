#include <vector>
#include <sys/types.h>
#include <string>
#include "cmd.hpp"
#include "user.hpp"
#ifndef EXEC_HPP
#define EXEC_HPP
pid_t execsingle(const std::vector<command>& cmds,int fdin,int fdout,user* users,int id,pid_t serverpid,const std::string& fullline);
pid_t execpipe(const std::vector<command>& cmds,int fdin,int fdout,user* users,int id,pid_t serverpid,const std::string& fullline);
#endif
