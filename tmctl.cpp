#include <unistd.h>
static bool interactive;
bool isinteractive(){
    return interactive;
}
void chkterm(){
    if(tcsetpgrp(STDIN_FILENO,getpid())==-1)interactive=false;
    else interactive=true;
}
void settermpgid(pid_t pid){
    if(interactive&&tcsetpgrp(STDOUT_FILENO,pid)==-1)
        throw("Error setting terminal pgrp");
}
