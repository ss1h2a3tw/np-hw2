#include <vector>
#include <sys/types.h>
#include <sys/wait.h>
#include <utility>
#include <string>
#include <queue>
#include <iostream>
using namespace std;
static vector<pair<pid_t,string>> cpgid;
void sigchldhandler(int){
    //First handle the signal
    waitpid(-1,NULL,WNOHANG);
    //Check whether chld's pgrp is empty
    queue<size_t> toerase;
    for(size_t i = 0 ; i < cpgid.size() ; i ++){
        if(waitpid(-cpgid[i].first,NULL,WNOHANG)==-1&&errno==ECHILD){
            toerase.push(i);
        }
    }
    while(!toerase.empty()){
        cerr << endl << "Job " << toerase.front()+1 << ":" << cpgid[toerase.front()].second << " has ended" << endl;
        cpgid[toerase.front()].first=0;
        toerase.pop();
    }
}

size_t insertchld(pid_t pid,const string& exe){
    auto num=cpgid.size();
    for(size_t i = 0 ; i < num ; i ++){
        if(cpgid[i].first==0){
            num=i;
            cpgid[i].first=pid;
            cpgid[i].second=exe;
        }
    }
    if(num==cpgid.size()){
        cpgid.push_back({pid,exe});
    }
    return num;
}
pair<pid_t,string> getchld(size_t id){
    if(id>=cpgid.size())throw("Not a exist job num");
    if(cpgid[id].first==0)throw("Not a exist job num");
    return cpgid[id];
}
