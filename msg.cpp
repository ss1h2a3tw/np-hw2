#include "user.hpp"
#include <cstring>
#include <csignal>
#include <sys/types.h>
#include <cstdio>
using namespace std;
static int insertmsg(user* self,const char* msg){
    int slot=-1;
    for(int i = 0 ; i < 10 ; i ++){
        if(self->msg_to[i]==-2){
            strncpy(self->msg_ct[i],msg,MSG_LENGTH_MAX);
            slot=i;
            break;
        }
    }
    if(slot==-1){
        strncpy(self->msg_ct[9],msg,MSG_LENGTH_MAX);
        slot=9;
    }
    return slot;
}

void broadcast(user* self,const char* msg,pid_t serverpid){
    int slot=insertmsg(self,msg);
    self->msg_to[slot]=-1;
    kill(serverpid,SIGUSR1);
}

void unicast(user* self,const char* msg,int id,pid_t serverpid){
    int slot=insertmsg(self,msg);
    self->msg_to[slot]=id;
    kill(serverpid,SIGUSR1);
}
