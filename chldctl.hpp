#include <string>
#include <sys/types.h>
#include <utility>
#ifndef CHLDCTL_HPP
#define CHLDCTL_HPP
void sigchldhandler(int);
size_t insertchld(pid_t pid,const std::string& exe);
std::pair<pid_t,std::string> getchld(size_t id);
#endif
