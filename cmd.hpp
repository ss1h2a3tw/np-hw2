#include <vector>
#include <string>
#ifndef CMD_HPP
#define CMD_HPP
const std::vector<std::string> SEPR = {">>",">&","<&","<",">","&","|","!",";"};
const std::vector<std::string> RDIR = {">>",">&","<&",">","<"};
enum rdir_type {APPEND_OUT,DUP_OUT,DUP_IN,OUT,IN,REMOTE_IN,REMOTE_OUT};
const std::vector<std::string> BLTIN = {"cd","setenv","unset","printenv","exit","fg","who","tell","yell","name"};
enum bltn_type {CD,SETENV,UNSET,PRINTENV,EXIT,FG,WHO,TELL,YELL,NAME};
//TODO redirect filedescripter [n]> word
//TODO << redirect
//TODO fg
const std::vector<std::string> PIPE = {"|","!"};
enum pipe_type {PIPE_OUT,PIPE_OUTERR};

struct redirect{
    rdir_type type;
    std::string path;
};

struct command{
    std::string exe;
    std::vector<std::string> args;
    std::vector<redirect> redirects;
    bool piped;
    pipe_type piped_type;
    size_t pipecnt;
    bool builtin;
    bltn_type builtin_type;
    bool background;
    void clear(){
        exe="";
        args.clear();
        redirects.clear();
        piped=false;
        pipecnt=0;
        background=false;
        builtin=false;
    }
};
#endif
