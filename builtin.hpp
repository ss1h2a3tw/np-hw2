#include "cmd.hpp"
#include "numberpipe.hpp"
#include "user.hpp"
#ifndef BUILTIN_HPP
#define BUILTIN_HPP
int ckrunbuiltin(const command& cmd,np_type& np,user* users,int id,pid_t serverpid);
#endif
